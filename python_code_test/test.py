import paho.mqtt.client as mqtt

# Fonction callback appelée lorsque la connexion au broker MQTT est établie
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connexion établie au broker MQTT")
        # S'abonner au topic "hello"
        client.subscribe("montmp/1")
        client.subscribe("montmp/2")
        client.subscribe("montmp/3")
        client.subscribe("montmp/4")
        client.subscribe("montmp/5")
    else:
        print(f"Échec de la connexion au broker MQTT, code de retour : {rc}")

# Fonction callback appelée lorsqu'un message est reçu
def on_message(client, userdata, msg):
    print(f"Message reçu sur le topic '{msg.topic}': {msg.payload.decode()}")

# Créer un client MQTT
client = mqtt.Client()

# Définir les fonctions de callback
client.on_connect = on_connect
client.on_message = on_message

# Se connecter au serveur MQTT (test.mosquitto.org)
client.connect("test.mosquitto.org", 1883, 60)

# Démarrer la boucle de communication MQTT
client.loop_forever()
