// Author : Hicham
// V1.0.1 Code de - machine de moore

#include "M5StickCPlus.h"
#include <WiFi.h>
#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);

// Configure the name and password of the connected wifi and your MQTT Serve
const char *ssid = "Bbox-CF130F21";
const char *password = "Hicham123";
const char *mqtt_server = "test.mosquitto.org";

void setupWifi(); // Connection WIFI
void reConnect(); // Reconnect en cas de pertes du signal

unsigned char detect_face();                                           // detection numéro de face
unsigned long number_of_second(int heures, int minutes, int secondes); // convertion 00:00:00 en seconde
void Count_time(unsigned char face, unsigned long time);               //  incrementation du temps passé sur une face

char *longToString(unsigned long num); // parsing long to char*

// Etat possiblede la machine
enum ETAT
{
  IDLE_STATE,
  START_COUNT,
  WAIT_FOR_CONNECTION,
  CONNECTED_TO_WIFI,
  CONNECTING_TO_BROKER,
  CONNECTED_TO_BROKER
};

ETAT etat_courant;

// FLAGS
bool Conneted_FLAG = false;         // Connected to wifi
bool Client_Connected_FLAG = false; // Connected to broker
bool ButtonA_Pressed_FLAG = false;  //
bool ButtonA_Pressed_700_FLAG = false;
bool Publish_finished_FLAG = false;

// variable IMU accel
float accX = 0;
float accY = 0;
float accZ = 0;

unsigned char ancienne_face = 0; // face veille pour init
unsigned char Face_detecte = 0;

RTC_TimeTypeDef RTC_TimeStruct; // RTC struct

unsigned long Tab_Temp[6]; // tableau compteur
unsigned long Temp_ecoule = 0;

void setup()
{
  M5.begin();
  M5.Imu.Init(); // Init IMU.
  Serial.begin(115200);
  etat_courant = IDLE_STATE;   // init state
  M5.Lcd.setTextColor(YELLOW); // Set the font color to yellow.
  M5.Lcd.setTextSize(2);       // Set the font size to 2.
  M5.Lcd.setRotation(3);
  M5.Lcd.println("Code test DE");
}

// le cycle loop respecte le cycle automate :: LECTURE ENTEES -- TRAITEMENT ETAT -- GESTION SORTIES

void loop()
{

  // LECTURE ENTEES

  M5.update();
  // lecture bouton poussoir
  if (M5.BtnA.wasReleased())
  {
    Serial.println("A Pressed");
    ButtonA_Pressed_FLAG = true;
    M5.Lcd.println("Start Count");
  }
  else if (M5.BtnA.wasReleasefor(700))
  {
    Serial.println("A Pressed for 700ms");
    ButtonA_Pressed_700_FLAG = true;
  }

  // Lecture face actuelle
  Face_detecte = detect_face();

  // TRAITEMENT ETAT

  switch (etat_courant)
  {
  case IDLE_STATE:
    Serial.println("IDLE");
    if (ButtonA_Pressed_FLAG)
    {
      ButtonA_Pressed_FLAG = false; // reset
      etat_courant = START_COUNT;
    }
    break;
  case START_COUNT:
    Serial.println("SC");
    if (ButtonA_Pressed_700_FLAG)
    {
      ButtonA_Pressed_700_FLAG = false; // reset
      etat_courant = WAIT_FOR_CONNECTION;
    }
    break;
  case WAIT_FOR_CONNECTION:
    Serial.println("WFW");
    if (Conneted_FLAG)
    {
      etat_courant = CONNECTED_TO_WIFI;
    }
    break;
  case CONNECTED_TO_WIFI:
    Serial.println("CF");
    etat_courant = CONNECTING_TO_BROKER;
    break;
  case CONNECTING_TO_BROKER:
    Serial.println("CgTB");
    if (Client_Connected_FLAG)
      etat_courant = CONNECTED_TO_BROKER;
    break;
  case CONNECTED_TO_BROKER:
    Serial.println("CTB");
    if (Publish_finished_FLAG)
    {
      Publish_finished_FLAG = false; // reset
      etat_courant = IDLE_STATE;
    }
    break;
  default:
    break;
  }

  // GESTION SORTIES

  switch (etat_courant)
  {
  case IDLE_STATE:
    break;
  case START_COUNT:
    Serial.println(Face_detecte);
    Serial.println(ancienne_face);
    if (Face_detecte == ancienne_face) // test si la face change
    {
      Serial.println("no change");
    }
    else
    {
      // comptabilisation du temp passe
      M5.Rtc.GetTime(&RTC_TimeStruct);
      Temp_ecoule = number_of_second(RTC_TimeStruct.Hours, RTC_TimeStruct.Minutes, RTC_TimeStruct.Seconds);
      Count_time(ancienne_face, Temp_ecoule);
      Temp_ecoule = 0; // reset

      // reset TIME =0
      RTC_TimeTypeDef TimeStruct;
      TimeStruct.Hours = 0; // Set the time.
      TimeStruct.Minutes = 0;
      TimeStruct.Seconds = 0;
      M5.Rtc.SetTime(&TimeStruct); // writes the set time to the real time clock.

      ancienne_face = Face_detecte;
    }
    break;
  case WAIT_FOR_CONNECTION:
    M5.Lcd.println("Connecting");
    setupWifi(); // se connecter - fonction bloquante
    Conneted_FLAG = true;
    break;
  case CONNECTED_TO_WIFI:
    break;
  case CONNECTING_TO_BROKER:
    client.setServer(mqtt_server, 1883); // Sets the server details.
    Client_Connected_FLAG = true;
    if (client.connected())
    {
      Client_Connected_FLAG = true;
    }
    break;
  case CONNECTED_TO_BROKER:
    if (!client.connected())
    {
      reConnect();
    }
    Serial.print("Publish message: ");
    client.publish("montmp/1", longToString(Tab_Temp[1])); // Publishes a message to the specified
    client.publish("montmp/2", longToString(Tab_Temp[2])); // Publishes a message to the specified
    client.publish("montmp/3", longToString(Tab_Temp[3])); // Publishes a message to the specified
    client.publish("montmp/4", longToString(Tab_Temp[4])); // Publishes a message to the specified
    client.publish("montmp/5", longToString(Tab_Temp[5])); // Publishes a message to the specified
    Publish_finished_FLAG = true;
    break;

  default:
    break;
  }

  delay(200); // delai entre chaque cycle
}

void setupWifi()
{
  delay(10);
  WiFi.mode(WIFI_STA);        // Set the mode to WiFi station mode.
  WiFi.begin(ssid, password); // Start Wifi connection.

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.printf("\nSuccess\n");
}

void reConnect()
{
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID.
    String clientId = "M5Stack-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect.
    if (client.connect(clientId.c_str()))
    {
      Serial.printf("\nSuccess Reconnection\n");
      // Once connected, publish an announcement to the topic.
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println("try again in 5 seconds");
      delay(5000);
    }
  }
}

unsigned char detect_face()
{
  M5.IMU.getAccelData(&accX, &accY, &accZ);
  // Serial.println("***************************************************");
  // Serial.printf(" X= %5.2f   Y= %5.2f   Z= %5.2f\n", accX, accY, accZ);

  if (-0.2 <= accX && accX <= 0.2 &&
      -0.2 <= accY && accY <= 0.2 &&
      -1.2 <= accZ && accZ <= -0.8)
  {
    Serial.println("face1");
    return 1;
  }
  else if (-0.2 <= accX && accX <= 0.2 &&
           0.8 <= accY && accY <= 1.2 &&
           -0.2 <= accZ && accZ <= 0.2)
  {
    Serial.println("face2");
    return 2;
  }
  else if ((-1.2 <= accX && accX <= -0.8) &&
           (-0.2 <= accY && accY <= 0.2) &&
           (-0.2 <= accZ && accZ <= 0.2))
  {
    Serial.println("face3");
    return 3;
  }
  else if ((0.8 <= accX && accX <= 1.2) &&
           (-0.2 <= accY && accY <= 0.2) &&
           (-0.2 <= accZ && accZ <= 0.2))
  {
    Serial.println("face4");
    return 4;
  }
  else if ((-0.2 <= accX && accX <= 0.2) &&
           (-1.2 <= accY && accY <= -0.8) &&
           (-0.2 <= accZ && accZ <= 0.2))
  {
    Serial.println("face5");
    return 5;
  }
  else if ((-0.2 <= accX && accX <= 0.2) &&
           (-0.2 <= accY && accY <= 0.2) &&
           (0.8 <= accZ && accZ <= 1.2))
  {
    Serial.println("face veille");
    return 0;
  }
  else
  {
    Serial.println("Aucune face detecte");
    return 66;
  }
}

unsigned long number_of_second(int heures, int minutes, int secondes)
{
  return (heures * 3600) + (minutes * 60) + secondes;
}

void Count_time(unsigned char face, unsigned long time)
{
  switch (face)
  {
  case 1:
    Tab_Temp[1] += time;
    break;
  case 2:
    Tab_Temp[2] += time;
    break;
  case 3:
    Tab_Temp[3] += time;
    break;
  case 4:
    Tab_Temp[4] += time;
    break;
  case 5:
    Tab_Temp[5] += time;
    break;
  case 0:
    break;
  default:
    break;
  }
}

char *longToString(unsigned long num)
{
  char *str = (char *)malloc(20); // Allouer de la mémoire pour la chaîne (assez grande pour contenir un long)
  if (str == NULL)
  {
    perror("Erreur d'allocation mémoire");
    exit(EXIT_FAILURE);
  }
  sprintf(str, "%ld", num); // Convertir le long en chaîne de caractères
  return str;
}
