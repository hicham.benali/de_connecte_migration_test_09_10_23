
#include <M5StickCPlus.h>

RTC_TimeTypeDef RTC_TimeStruct;
RTC_DateTypeDef RTC_DateStruct;


void setup() {
    Serial.begin(115200);
    M5.begin();                // Init M5StickC Plus.
    Serial.println("RTC TEST");


    RTC_TimeTypeDef TimeStruct;
    TimeStruct.Hours   = 0;  // Set the time.  
    TimeStruct.Minutes = 0;
    TimeStruct.Seconds = 0;
    M5.Rtc.SetTime(&TimeStruct);  // writes the set time to the real time clock.
                                  
    RTC_DateTypeDef DateStruct;
    DateStruct.WeekDay = 1;  // Set the date.  
    DateStruct.Month   = 10;
    DateStruct.Date    = 9;
    DateStruct.Year    = 2023;
    M5.Rtc.SetDate(&DateStruct);  // writes the set date to the real time clock.

}

void loop() {
  M5.Rtc.GetTime(&RTC_TimeStruct);
  M5.Rtc.GetDate(&RTC_DateStruct);
  Serial.printf("Date : %04d-%02d-%02d\n", RTC_DateStruct.Year,RTC_DateStruct.Month, RTC_DateStruct.Date);
  Serial.printf("Week : %d\n", RTC_DateStruct.WeekDay);
  Serial.printf("Time : %02d : %02d : %02d\n", RTC_TimeStruct.Hours,RTC_TimeStruct.Minutes, RTC_TimeStruct.Seconds);
  delay(5000);

}
