
#include "M5StickCPlus.h"
#include <WiFi.h>
#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);

// Configure the name and password of the connected wifi and your MQTT Serve
const char* ssid        = "Bbox-CF130F21";
const char* password    = "Hicham123";
const char* mqtt_server = "test.mosquitto.org";

unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

void setupWifi();
void callback(char* topic, byte* payload, unsigned int length);
void reConnect();

void setup() {
    M5.begin();
    setupWifi();
    client.setServer(mqtt_server,1883);  // Sets the server details. 
    client.setCallback(callback);  // Sets the message callback function.  
}

void loop() {
    if (!client.connected()) {
        reConnect();
    }
    client.loop();  // This function is called periodically to allow clients to
                    // process incoming messages and maintain connections to the
                    // server.
   

    unsigned long now =
        millis();  // Obtain the host startup duration.  
    if (now - lastMsg > 2000) {
        lastMsg = now;
        ++value;
        snprintf(msg, MSG_BUFFER_SIZE, "hello world #%ld",value);  // Format to the specified string and store it in MSG.
                         
        Serial.print("Publish message: ");
        Serial.println(msg);
        client.publish("M5Stack", msg);  // Publishes a message to the specified
                                         
    }
}

void setupWifi() {
    delay(10);
    WiFi.mode(WIFI_STA);  // Set the mode to WiFi station mode.  
    WiFi.begin(ssid, password);  // Start Wifi connection.  

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
   Serial.printf("\nSuccess\n");
}

void callback(char* topic, byte* payload, unsigned int length) {
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println();
}

void reConnect() {
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Create a random client ID.  
        String clientId = "M5Stack-";
        clientId += String(random(0xffff), HEX);
        // Attempt to connect.  
        if (client.connect(clientId.c_str())) {
            Serial.printf("\nSuccess\n");
            // Once connected, publish an announcement to the topic.
           
            client.publish("M5Stack", "hello world");
            // ... and resubscribe.  
            client.subscribe("M5Stack");
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println("try again in 5 seconds");
            delay(5000);
        }
    }
}
