#include <Arduino.h>
#include <M5StickCPlus.h>


float accX = 0;
float accY = 0;
float accZ = 0;

float gyroX = 0;
float gyroY = 0;
float gyroZ = 0;

float pitch = 0;
float roll  = 0;
float yaw   = 0;



void setup() {
  // Initialisation du port série pour la communication avec le moniteur série
  Serial.begin(115200);
  M5.begin();                // Init M5StickC Plus.
  M5.Imu.Init();             // Init IMU.
 Serial.println("MPU6886 TEST");
}

void loop() {
 
    M5.IMU.getGyroData(&gyroX, &gyroY, &gyroZ);
    M5.IMU.getAccelData(&accX, &accY, &accZ);
    M5.IMU.getAhrsData(&pitch, &roll, &yaw);
    Serial.println("***************************************************");
    Serial.printf(" X= %6.2f   Y= %6.2f   Z= %6.2f\n", gyroX, gyroY, gyroZ);
    Serial.printf(" X= %5.2f   Y= %5.2f   Z= %5.2f\n", accX, accY, accZ);
    Serial.printf(" P= %5.2f   R= %5.2f   Y= %5.2f\n", pitch, roll, yaw);

 

   if (-0.2 <= accX && accX <= 0.2 &&
    -0.2 <= accY && accY <= 0.2 &&
    -1.2 <= accZ && accZ <= -0.8)
    {
            Serial.println("face1");
    }
  else if (-0.2 <= accX && accX <= 0.2 &&
         0.8 <= accY && accY <= 1.2 &&
         -0.2 <= accZ && accZ <= 0.2) 
    {
            Serial.println("face2");
    }
  else if ((-1.2 <= accX && accX <= -0.8) &&
         (-0.2 <= accY && accY <= 0.2) &&
         (-0.2 <= accZ && accZ <= 0.2)) 
     {
           Serial.println("face3");
     }            
  else if ((0.8 <= accX && accX <= 1.2) &&
         (-0.2 <= accY && accY <= 0.2) &&
         (-0.2 <= accZ && accZ <= 0.2)) 
     { 
          Serial.println("face4");
     }        
  else if ((-0.2 <= accX && accX <= 0.2) &&
         (-1.2 <= accY && accY <= -0.8) &&
         (-0.2 <= accZ && accZ <= 0.2)) 
       {
            Serial.println("face5");
       }     
  else if ((-0.2 <= accX && accX <= 0.2) &&
         (-0.2 <= accY && accY <= 0.2) &&
         (0.8 <= accZ && accZ <= 1.2)) 
      {
            Serial.println("face veille");
      }
   else
   {
            Serial.println("Aucune face detecte");
   }
    delay(5000);
}
